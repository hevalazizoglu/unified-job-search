package crawler;

import models.JobHtmlElement;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.nio.file.Paths;

public class Indexer {

    private String indexDirectory;
    private boolean isCreate;

    private IndexWriter writer;

    public Indexer(String indexDirectory, boolean isCreate) {
        this.indexDirectory = indexDirectory;
        this.isCreate = isCreate;

        try {
            Directory dir = FSDirectory.open(Paths.get(this.indexDirectory));

            Analyzer analyzer = new StandardAnalyzer();

            IndexWriterConfig iwc = new IndexWriterConfig(analyzer);

            if (this.isCreate) {
                // Create a new index in the directory, removing any
                // previously indexed documents:
                iwc.setOpenMode(OpenMode.CREATE);
            } else {
                // Add new documents to an existing index:
                iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
            }

            writer = new IndexWriter(dir, iwc);

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public synchronized void indexWebPage(String text, String title, String url) {
        Document doc = new Document();
        Field urlField = new StringField("url", url, Field.Store.YES);
        doc.add(urlField);
        Field textField = new TextField("text", text, Field.Store.YES);
        doc.add(textField);
        Field titleField = new TextField("title", title, Field.Store.YES);
        doc.add(titleField);
        if (writer.getConfig().getOpenMode() == OpenMode.CREATE) {
            System.out.println("indexing:   " + url + " => " + title);
            try {
                writer.addDocument(doc);
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    public synchronized void indexWebPage(JobHtmlElement jobHtmlElement) {
        if (jobHtmlElement.getUrl() == null || jobHtmlElement.getTitle() == null)
            return;

        Document doc = new Document();
        Field urlField = new StringField("url", jobHtmlElement.getUrl(), Field.Store.YES);
        doc.add(urlField);
        Field titleField = new TextField("title", jobHtmlElement.getTitle(), Field.Store.YES);
        doc.add(titleField);
        Field textField = new TextField("text", jobHtmlElement.getText(), Field.Store.YES);
        doc.add(textField);
        Field companyField = new TextField("company", jobHtmlElement.getCompany(), Field.Store.YES);
        doc.add(companyField);
        Field locationField = new TextField("location", jobHtmlElement.getLocation(), Field.Store.YES);
        doc.add(locationField);

        if (writer.getConfig().getOpenMode() == OpenMode.CREATE) {
            System.out.println("indexing:   " + jobHtmlElement.getUrl() + " => " + jobHtmlElement.getTitle());
            try {
                writer.addDocument(doc);
            } catch (Exception e) {
            }
        }
    }

    public void close() {
        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}