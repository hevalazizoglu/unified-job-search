package crawler;

import models.JobHtmlElement;
import org.jsoup.nodes.Document;
import parsers.WebPageParser;
import parsers.WebPageParserFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;

public class WebCrawler extends Thread {

    // used to keep examined web pages
    private static ConcurrentHashMap<String, Boolean> visited = new ConcurrentHashMap<String, Boolean>();

    // starting points to crawl
    private String startingPoint = null;

    // list of web pages to be examined
    private Queue<JobHtmlElement> queue = null;

    // max depth to crawl
    private int depth = 0;

    private Indexer indexer = null;

    public WebCrawler(String startingPoint, int depth, Indexer indexer) {
        this.startingPoint = startingPoint;
        this.depth = depth;
        this.indexer = indexer;

        this.queue = new LinkedList<>();
        JobHtmlElement jobHtmlElement = new JobHtmlElement();
        jobHtmlElement.setUrl(this.startingPoint);
        this.queue.offer(jobHtmlElement);
        this.queue.offer(null);
    }

    public void run() {
        // setUp();

        // breadth first search crawl of web
        while (!queue.isEmpty() && depth > 0) {
            JobHtmlElement jobHtmlElement = queue.poll();

            // level completed and jump to next level
            if (jobHtmlElement == null) {
                depth--;
                queue.offer(null);
            } else {
                try {
                    examinePage(jobHtmlElement);
                } catch (Exception e) {
                    //e.printStackTrace();
                }
            }
        }
    }

    private void examinePage(JobHtmlElement jobHtmlElementArg) {

        try {

            WebPageParser pageParser = new WebPageParserFactory().getParser(jobHtmlElementArg.getUrl());

            Document document = pageParser.getDocument();

            processPage(document, jobHtmlElementArg);

            List<JobHtmlElement> jobHtmlElements = pageParser.parse();

            if (jobHtmlElements == null)
                return;

            for (JobHtmlElement jobHtmlElement : jobHtmlElements) {

                if (jobHtmlElement.getUrl() != null && !visited.containsKey(jobHtmlElement.getUrl())) {

                    visited.put(jobHtmlElement.getUrl(), true);

                    if (queue.size() > WebCrawlerConf.MAX_DOCUMENT_NUMBER)
                        return;

                    queue.offer(jobHtmlElement);
                }
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }

    }

    private void processPage(Document document, JobHtmlElement jobHtmlElementArg) {
        System.out.println(".... processing page = " + jobHtmlElementArg.getUrl());
        jobHtmlElementArg.setTitle(document.title());
        jobHtmlElementArg.setText(document.body().text());
        this.indexer.indexWebPage(jobHtmlElementArg);
    }

    private void setUp() {
        // timeout connection after 500 miliseconds
        System.setProperty("sun.net.client.defaultConnectTimeout", "500");
        System.setProperty("sun.net.client.defaultReadTimeout", "1000");
    }

    private void print(String msg, Object... args) {
        System.out.println(String.format(msg, args));
    }

    private String trim(String s, int width) {
        if (s.length() > width)
            return s.substring(0, width - 1) + ".";
        else
            return s;
    }

}