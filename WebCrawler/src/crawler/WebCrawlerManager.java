package crawler;

public class WebCrawlerManager {


    public static void main(String[] args) {

        // allocate web crawler instances for each of starting points
        WebCrawler[] webCrawlers = new WebCrawler[WebCrawlerConf.STARTING_POINTS.length];

        // the directory will contain the indexes
        String index = WebCrawlerConf.INDEX_PATH;

        // all threads will use same indexer
        Indexer indexer = new Indexer(index, WebCrawlerConf.INDEX_CREATE);

        for (int i = 0; i < webCrawlers.length; ++i) {
            WebCrawler webCrawler = new WebCrawler(WebCrawlerConf.STARTING_POINTS[i], WebCrawlerConf.CRAWL_DEPTH, indexer);
            try {
                webCrawler.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            webCrawlers[i] = webCrawler;
        }

        // start crawling
        for (WebCrawler webCrawler : webCrawlers) {
            webCrawler.run();
        }

        // close index writer
        indexer.close();

        System.out.println("All finished successfully");
    }
}