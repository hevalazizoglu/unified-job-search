package crawler;


public class WebCrawlerConf {


    public static final int CRAWL_DEPTH = 5;

    public static final int MAX_DOCUMENT_NUMBER = 500;

    // the directory will contain the indexes
    public static final String INDEX_PATH = "index";

    public static final boolean INDEX_CREATE = true;

    // initial web pages
    public static final String[] STARTING_POINTS = {
            "http://tr.indeed.com/software-jobs",
            "http://www.monster.com/jobs/search/?q=Software-Engineer",
            "http://www.simplyhired.com/search?q=software+engineer&l="
    };
}
