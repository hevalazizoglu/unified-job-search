package crawler;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;

import java.io.IOException;
import java.nio.file.Paths;

public class QueryEngine {

    private String indexDirectory;
    private IndexReader reader;
    private IndexSearcher searcher;
    private Analyzer analyzer;
    private QueryParser parser;

    private MultiFieldQueryParser multiFieldQueryParser;

    public QueryEngine(String indexPath) {
        this.indexDirectory = indexPath;

        try {
            reader = DirectoryReader.open(FSDirectory.open(Paths.get(this.indexDirectory)));
            searcher = new IndexSearcher(reader);
            analyzer = new StandardAnalyzer();
            parser = new QueryParser("title", analyzer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void searchByTitle(String jobTitle) throws ParseException {
        searchByTitleAndLocation(jobTitle, null);
    }

    public void searchByLocation(String location) throws ParseException {
        searchByTitleAndLocation(null, location);
    }

    public void searchByTitleAndLocation(String jobTitle, String location) throws ParseException {
        try {
            String queryString = constructQueryString(jobTitle, location);

            /*
            Query query1 = new TermQuery(new Term("text", location));
            Query query2 = new TermQuery(new Term("title", jobTitle));

            BooleanQuery booleanQuery = new BooleanQuery.Builder()
                                        .add(query1, BooleanClause.Occur.SHOULD)
                                        .add(query2, BooleanClause.Occur.SHOULD).build();
            */

            Query query = parser.parse(queryString);
            TopDocs results = searcher.search(query, 100);
            ScoreDoc[] hits = results.scoreDocs;
            System.out.println("Number of results: " + hits.length);
            for (ScoreDoc h : hits) {
                Document doc = searcher.doc(h.doc);

                System.out.println(doc.getField("title").stringValue());
                System.out.println(doc.getField("url").stringValue());
                System.out.println(h);
                System.out.println();
            }
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String constructQueryString(String title, String location) {
        StringBuilder sb = new StringBuilder();
        if (title != null) {
            sb.append("title:");
            sb.append(title);
        }
        if (location != null) {
            if (sb.length() > 0)
                sb.append(" AND ");
            sb.append("text:");
            sb.append(location);
        }
        return sb.toString();
    }
}