package parsers;

import models.JobHtmlElement;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;


public class SimplyHired extends WebPageParser {

    public SimplyHired(String url) {
        super(url);
    }

    @Override
    public List<JobHtmlElement> parse() {
        List<JobHtmlElement> jobHtmlElements = new ArrayList<>();

        Document doc = getDocument();

        Elements rowResults = doc.select(".card.js-job");

        for (Element row : rowResults) {
            JobHtmlElement jobHtmlElement = new JobHtmlElement();

            String company = row.getElementsByClass("serp-company").text();
            String location = row.getElementsByClass("serp-location").text();

            String link = row.select(".card-link.js-job-link").attr("content");

            System.out.println("---->    Simply Hired: " + company + " => " + link);

            jobHtmlElement.setUrl(link);
            jobHtmlElement.setCompany(company);
            jobHtmlElement.setLocation(location);

            jobHtmlElements.add(jobHtmlElement);
        }

        List<JobHtmlElement> jobHtmlElementsLinks = getAllLinksInTheDocument();

        if (jobHtmlElementsLinks != null)
            jobHtmlElements.addAll(jobHtmlElementsLinks);


        return jobHtmlElements;
    }
}
