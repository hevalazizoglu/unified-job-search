package parsers;

import models.JobHtmlElement;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;


public class IndeedParser extends WebPageParser {

    public IndeedParser(String url) {
        super(url);
    }

    @Override
    public List<JobHtmlElement> parse() {
        List<JobHtmlElement> jobHtmlElements = new ArrayList<>();

        Document doc = getDocument();

        // test
        Elements rowResults = doc.select(".row.result");
        for (Element row : rowResults) {
            JobHtmlElement jobHtmlElement = new JobHtmlElement();

            String company = row.getElementsByClass("company").text();
            String location = row.getElementsByClass("location").text();

            Elements link = row.select("a[href]");
            String href = link.attr("abs:href");

            jobHtmlElement.setUrl(href);
            jobHtmlElement.setCompany(company);
            jobHtmlElement.setLocation(location);

            jobHtmlElements.add(jobHtmlElement);
        }

        List<JobHtmlElement> jobHtmlElementsLinks = getAllLinksInTheDocument();

        if (jobHtmlElementsLinks != null)
            jobHtmlElements.addAll(jobHtmlElementsLinks);

        return jobHtmlElements;
    }
}
