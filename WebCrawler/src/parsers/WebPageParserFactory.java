package parsers;


public class WebPageParserFactory {

    public WebPageParser getParser(String url) {
        if (url.contains("indeed")) {
            return new IndeedParser(url);
        } else if (url.contains("monster.com") && !url.contains("jobview.monster.com")) {
            return new MonsterParser(url);
        } else if (url.contains("simplyhired.com")) {
            return new SimplyHired(url);
        }

        return new WebPageParser(url);
    }
}
