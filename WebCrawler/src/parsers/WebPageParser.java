package parsers;

import models.JobHtmlElement;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class WebPageParser {

    protected String url;
    protected Document document;

    public WebPageParser(String url) {
        this.url = url;
    }

    public Document getDocument() {
        if (this.document == null)
            this.document = connect();
        return this.document;
    }

    public List<JobHtmlElement> parse() {
        this.document = getDocument();

        if (this.document == null)
            return null;

        return getAllLinksInTheDocument();
    }

    public List<JobHtmlElement> getAllLinksInTheDocument() {

        List<JobHtmlElement> jobHtmlElements = new ArrayList<>();

        // get all links
        Elements links = this.document.select("a[href]");

        for (Element link : links) {

            String href = link.attr("abs:href");
            href = getFullUrlFromHref(href, this.document.baseUri());

            JobHtmlElement jobHtmlElement = new JobHtmlElement();

            jobHtmlElement.setUrl(href);

            jobHtmlElements.add(jobHtmlElement);
        }

        return jobHtmlElements;
    }


    protected Document connect() {
        Connection connection = Jsoup.connect(this.url).userAgent("Chrome").timeout(10000).ignoreHttpErrors(true)
                .ignoreContentType(true).followRedirects(true).validateTLSCertificates(false);

        try {
            Connection.Response resp = connection.execute();

            if (resp.statusCode() == 200) {

                Document doc = connection.get();

                return doc;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    protected String trim(String s, int width) {
        if (s.length() > width)
            return s.substring(0, width - 1) + ".";
        else
            return s;
    }

    protected String getFullUrlFromHref(String href, String url) {
        if (href.startsWith("mailto")) {
            return null;
        }
        if (href.startsWith("http")) {
            return href;
        }
        if (href.contains("#")) {
            href = href.substring(0, href.indexOf("#"));
        }

        URL fullUrl = null;
        try {
            fullUrl = new URL(new URL(url), href);
        } catch (MalformedURLException e) {
            // e.printStackTrace();
        }
        return fullUrl.toString();
    }
}