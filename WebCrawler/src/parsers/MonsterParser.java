package parsers;

import models.JobHtmlElement;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;


public class MonsterParser extends WebPageParser {

    public MonsterParser(String url) {
        super(url);
    }

    @Override
    public List<JobHtmlElement> parse() {
        List<JobHtmlElement> jobHtmlElements = new ArrayList<>();

        Document doc = getDocument();

        Elements scriptElements = doc.getElementsByTag("script");

        JSONParser parser = new JSONParser();

        System.out.println("Monster Parse Page: " + url);
        for (Element element : scriptElements) {

            try {
                Object obj = parser.parse(element.data());
                JSONObject jsonObject = (JSONObject) obj;

                if (jsonObject.get("title") != null) {
                    JSONObject hiringOrganization = (JSONObject) jsonObject.get("hiringOrganization");
                    JSONObject jobLocation = (JSONObject) jsonObject.get("jobLocation");
                    String location = (String) jobLocation.get("addressLocality") + " - " + (String) jobLocation.get("addressRegion");

                    JobHtmlElement jobHtmlElement = new JobHtmlElement();
                    jobHtmlElement.setUrl((String) jsonObject.get("url"));
                    jobHtmlElement.setTitle((String) jsonObject.get("title"));
                    jobHtmlElement.setCompany((String) hiringOrganization.get("name"));
                    jobHtmlElement.setLocation(location);

                    //System.out.println("-------------------");

                    jobHtmlElements.add(jobHtmlElement);
                }

            } catch (ParseException e) {

            }
        }

        List<JobHtmlElement> jobHtmlElementsLinks = getAllLinksInTheDocument();

        for (JobHtmlElement jobHtmlElement : jobHtmlElementsLinks) {
            System.out.println("=>>> " + jobHtmlElement.getUrl());
        }

        if (jobHtmlElementsLinks != null)
            jobHtmlElements.addAll(jobHtmlElementsLinks);

        return jobHtmlElements;
    }
}
