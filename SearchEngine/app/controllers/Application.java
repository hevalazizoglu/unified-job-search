package controllers;

import conf.QueryConf;
import models.Job;
import models.JobBuilder;
import play.*;
import play.mvc.*;

import services.QueryService;
import views.html.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.queryparser.classic.ParseException;


public class Application extends Controller {

    public Result index() {
        return ok(index.render());
    }

    public Result search() {
        String title = request().getQueryString("q");

        String location = request().getQueryString("where");

        String pageStr = request().getQueryString("page");

        Integer currentPage = 1;
        Integer maxPageNumber;

        if(pageStr != null){
            try {
                currentPage = Integer.parseInt(pageStr);
                if(currentPage < 1)
                    currentPage = 1;
            } catch (Exception e){
                currentPage = 1;
            }
        }

        List<Job> jobList = getMatchJobs(title, location);

        int offset = (currentPage-1) * QueryConf.PAGE_SIZE;

        if(jobList.size() % QueryConf.PAGE_SIZE == 0){
            maxPageNumber = jobList.size() / QueryConf.PAGE_SIZE;
        } else {
            maxPageNumber = jobList.size() / QueryConf.PAGE_SIZE + 1;
        }

        int fromIndex = offset;
        int toIndex = offset + QueryConf.PAGE_SIZE;

        if(toIndex >= jobList.size()){
            toIndex = jobList.size();
        }

        if(fromIndex > toIndex) {
            fromIndex = toIndex - QueryConf.PAGE_SIZE;
            currentPage = maxPageNumber;
        }

        String resultHeadline = "Showing " + (fromIndex + 1) + "-" + toIndex + " of " + jobList.size() +  " jobs";


        return ok(jobs.render(title, location, jobList.subList(fromIndex, toIndex), currentPage, maxPageNumber, resultHeadline));
    }


    public List<Job> getMatchJobs(String title, String location){

        QueryService queryService = new QueryService(QueryConf.INDEX_PATH);

        List<Job> jobs = new ArrayList<Job>();

        try{
            jobs = queryService.searchByTitleAndLocation(title, location);
        }
        catch (ParseException e){

        }

        return jobs;
    }
}
