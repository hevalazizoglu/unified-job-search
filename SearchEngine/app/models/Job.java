package models;


public class Job {

    private String  title   ;
    private String  company ;
    private String  location;
    private String  text    ;
    private String  url     ;
    private Integer hits    ;

    public Job(String title, String  company, String  location, String text, String url, Integer hits){
        this.title = title;
        this.company = company;
        this.location = location;
        this.text = text;
        this.url = url;
        this.hits = hits;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getHits() {
        return hits;
    }

    public void setHits(Integer hits) {
        this.hits = hits;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
