package models;


public class JobBuilder {

    private String  title   ;
    private String  company ;
    private String  location;
    private String  text    ;
    private String  url     ;
    private Integer hits    ;


    public Job createJob() {
        return new Job(title, company, location, text, url, hits);
    }

    public JobBuilder setTitle(String title) {
        this.title = title;
        return this;
    }

    public JobBuilder setUrl(String url) {
        this.url = url;
        return this;
    }

    public JobBuilder setText(String text) {
        this.text = text;
        return this;
    }

    public JobBuilder setHits(Integer hits) {
        this.hits = hits;
        return this;
    }

    public JobBuilder setCompany(String company) {
        this.company = company;
        return this;
    }

    public JobBuilder setLocation(String location) {
        this.location = location;
        return this;
    }
}