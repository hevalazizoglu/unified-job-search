package services;

import conf.QueryConf;
import models.Job;
import models.JobBuilder;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.FSDirectory;
import play.Logger;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


public class QueryService {

    private final int JOB_CONTENT_LENGTH = 300;

    private String indexDirectory;
    private IndexReader reader;
    private IndexSearcher searcher;
    private Analyzer analyzer;
    private QueryParser parser;

    private MultiFieldQueryParser multiFieldQueryParser;

    public QueryService(String indexPath) {
        this.indexDirectory = indexPath;

        try {
            reader = DirectoryReader.open(FSDirectory.open(Paths.get(this.indexDirectory)));
            searcher = new IndexSearcher(reader);
            analyzer = new StandardAnalyzer();
            parser = new QueryParser("title", analyzer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Job> searchByTitle(String jobTitle, Integer pageNumber) throws ParseException {
        return searchByTitleAndLocation(jobTitle, null, pageNumber);
    }

    public List<Job> searchByLocation(String location, Integer pageNumber) throws ParseException {
        return searchByTitleAndLocation(null, location, pageNumber);
    }

    public List<Job> searchByTitleAndLocation(String jobTitle, String location, Integer pageNumber) throws ParseException {

        List<Job> jobs = new ArrayList<Job>();

        try {
            String queryString = constructQueryString(jobTitle, location);
            Query query = parser.parse(queryString);

            TopDocs results = searcher.search(query, QueryConf.PAGE_SIZE * pageNumber);
            //TopDocs results = searcher.search(constructBooleanQuery(jobTitle, location), QueryConf.MAX_RESULT);

            ScoreDoc[] hits = results.scoreDocs;

            int offset = (pageNumber-1) * QueryConf.PAGE_SIZE;

            for (int i = 0 ; i < QueryConf.PAGE_SIZE && (i + offset < hits.length); ++i) {


                ScoreDoc h = hits[i + offset];

                Document doc = searcher.doc(h.doc);

                Logger.debug(doc.getField("title").stringValue());
                Logger.debug(doc.getField("url").stringValue());
                Logger.debug(h.toString());

                String text = doc.getField("text").stringValue();

                jobs.add(new JobBuilder().setTitle(doc.getField("title").stringValue())
                        .setCompany(getCompanyFromDocument(doc))
                        .setLocation(getLocationFromDocument(doc, location))
                        .setText(text.substring(0, Math.min(text.length(), JOB_CONTENT_LENGTH)) + "...")
                        .setUrl(doc.getField("url").stringValue())
                        .createJob()
                );

            }

        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }

        return jobs;
    }

    public List<Job> searchByTitleAndLocation(String jobTitle, String location) throws ParseException {

        List<Job> jobs = new ArrayList<Job>();

        try {
            String queryString = constructQueryString(jobTitle, location);
            Query query = parser.parse(queryString);

            TopDocs results = searcher.search(query, QueryConf.MAX_RESULT);
            //TopDocs results = searcher.search(constructBooleanQuery(jobTitle, location), QueryConf.MAX_RESULT);

            ScoreDoc[] hits = results.scoreDocs;

            for (ScoreDoc h : hits) {

                Document doc = searcher.doc(h.doc);

                Logger.debug(doc.getField("title").stringValue());
                Logger.debug(doc.getField("url").stringValue());
                Logger.debug(h.toString());

                String text = doc.getField("text").stringValue();

                jobs.add(new JobBuilder().setTitle(doc.getField("title").stringValue())
                        .setCompany(getCompanyFromDocument(doc))
                        .setLocation(getLocationFromDocument(doc, location))
                        .setText(text.substring(0, Math.min(text.length(), JOB_CONTENT_LENGTH)) + "...")
                        .setUrl(doc.getField("url").stringValue())
                        .createJob()
                );

            }

        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }

        return jobs;
    }

    private String constructQueryString(String title, String location) {
        String query = "";
        if (title != null) {
            query += "( title:" + title + " OR " + "text:\"" + title + "\"" + ")";
        }
        if (location != null) {
            if (query.length() > 0)
                query += " AND ";
            query += "text:" + location;
        }
        Logger.debug("QUERY ===> " + query);
        query += " AND company:/.{1}.*/";
        return query;
    }

    private BooleanQuery constructBooleanQuery(String title, String location) throws ParseException {
        // final query
        BooleanQuery.Builder queryBuilder = new BooleanQuery.Builder();

        // title query
        if (title != null) {
            QueryParser titleQP = new QueryParser("title", analyzer);
            Query titleQuery = titleQP.parse(title);
            queryBuilder.add(titleQuery, BooleanClause.Occur.MUST);
        }

        // location query
        if (location != null) {
            QueryParser locationQP = new QueryParser("text", analyzer);
            Query locationQuery = locationQP.parse(location);
            queryBuilder.add(locationQuery, BooleanClause.Occur.MUST);
        }

        return queryBuilder.build();
    }

    private String getLocationFromDocument(Document doc, String location) {
        //TODO Retrieve location from doc
        return doc.getField("location").stringValue();
    }

    private String getCompanyFromDocument(Document doc) {
        //TODO Retrieve company name from doc
        return doc.getField("company").stringValue();
    }

}
