package conf;


public class QueryConf {

    // the directory will contain the indexes,
    // relative path to index directory which created by WebCrawler
    public static final String INDEX_PATH = "../index";

    //the maximum items retrieved after searching
    public static final int MAX_RESULT = 5000;

    public static final int PAGE_SIZE = 5;
}
